from django.contrib.auth.tokens import PasswordResetTokenGenerator



class ResetRequestTokenGenerator(PasswordResetTokenGenerator):  

    def _make_hash_value(self, user, timestamp):
        return (
            str(timestamp)
        )

reset_password_token = ResetRequestTokenGenerator()