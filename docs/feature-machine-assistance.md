# Maitag backend + Delvin.to frontend

Delvin.to was designed to support the Maya chatbot (WhatsApp?)

## Existing `delvin.to` backend services

1. ToolJet: opensource nocode frontend (self-hosted on Digital Ocean)
2. Wit.AI: multilingual NLU (by Facebook Research?)
3. HumanFirst: high quality continuously improved NLU (expensive)
4. Turn.io: opensource chatbot framework (Praekelt.org) compatible with WhatsApp 

### 2. Wit.AI

Wit.AI (Facebook Research) does multilingual intent tagging and classification using a supervised classifier.
It also has a decent FastText Nepali->English translator (as well as other rare languages).
Wit.AI was an open-source project but it's become increasingly difficult to self-host the service after Facebook bought them and started "maintaining" the GPL-licensed code.

### 3. HumanFirst

HumanFirst bills themselves as a tool for conversation designers to help them become NLU designers.
Here's their marketing copy:

"""
Increase the coverage and accuracy of your NLU on a continuous basis with incoming data. Discover new intents, improve existing ones with additional training examples and edge-cases from real-life conversations, and prioritize new dialogue flows based on volume of similar requests.
"""

Maria and Greg can provide more details about how they use the service.
It's expensive, so they do not use it as a "continuous" improvement tool.

## Delvin + MAITag approach

We will incrementally replace and improve upon the NLU services provided by [Wit.AI]() and [HumanFirst.AI]().
We will incrementally replace and improve upon the ToolJet frontend with Svelte + Django widgets.
The maitag service is currently hosted at [maitag.onrender.com]() and the code is at [gitlab.com/tangibleai/maitag]().
Greg and Rochdi are merging a Delvin Django app with the MAITag django app at [gitlab.com/tangibleai/django-delvin]().

To automatically classify new texts, a supervised ML model must be combined with a self-supervised model.
In the future this architecture will be extended to handle multi-label classification (intent tagging) and handle additional languages: Kenyan and French (for Nurse Nisa), Nepali (for Maya), Spanish & Chinese (for MOIA's Poly), etc.

## Idea and use case

The idea is to build a function that takes two inputs: an array of arrays of utterances and a new utterance, and decides whether the new utterance should go into one of the existing arrays of utterances or into a completely new array. 

This function will be useful in classifying new utterances unseen by our chatbots so that based on the classification of the new utterance, the chatbot can continue responding to the user without any hindrance. The user will never know that the chatbot didn't understand what he/she just said, and the chatbot will continue operating like everything's fine.

When the new intent is generated, a link will be generated which will contain a form asking what to do with it. The form will contain a textbox where the viewer can add responses to the new intent. This link can be sent to the host organizations or the developers.

## Data

The original input dataset contains 1094 human-labeled utterances in a CSV file.
An additional column should be added to record the `'label_status'`.

```python
>>> df = pd.read_csv('data/public/utterance_intent_pairs_revised.anon.csv')
>>> df
                            utterance               intent                     tags
0                            "*menu*"            main_menu            ['main menu']
1                       ", thank you"            thank_you            ['thank you']
2            "1. What is trafficking"  what_is_trafficking  ['what is trafficking']
3               "2. Safety behaviors"       safe_behaviors       ['safe behaviors']
4     "A question the system is no...          testing_you          ['testing you']
...                               ...                  ...                      ...
1089           Tell me more about it.          information          ['information']
1090             nepalese and english       both_languages       ['both languages']
1091   I'd like to ask you something.         ask_question         ['ask question']
1092                How was your day?          how_are_you          ['how are you']
1093                  tell me a story       stories_module       ['stories module']
```

There are 33 possible human-assigned intent categories (labels), and one 'unknown' label.
There are many duplicate utterances such that there are only 955 unique utterances.

```python
>>> df.describe(include='all')
       utterance   intent         tags
count       1094     1094         1094
unique       955       34           39
top       "<URL>  unknown  ['unknown']
freq          55      506          493
```

This data was recorded and labeled at the beginning of the year.
Additional data is available, including Nepali-English utterance pairs.
This data should be anonymized before it sharing publicly in the MAITAG repo and API.

There are currently approximately 20 label categories.
There are approximately 500 unlabeled utterances that are currently labeled as unknown.

The pipeline below should be designed to assist a human in labeling utterances with intents, one utterance at a time, not all at once.
It uses either an existing "final" (human-verified) label, or a new "tentative" machine-generated label for each new unknown utterance.
This pipeline has three kinds of intent labels:

1. `'unknown'`: not yet labeled by a human or machine
2. `'final'`: human verified
3. `'tentative'`: machine-generated label
4. `'machine'`: machine-assigned label (from set of `final` labels) that meets confidence threshold requirement (`SUPERVISED_PROBA_THRESH`)

And it uses 3 ML models.

1. SBERT - or any other encoder designed to create high-dimensional vector embeddings of 1 to 2 sentence utterances
2. LogisticRegression - or any other supervised classifier such as SVC or RandomForest
3. A custom unsupervised or self-supervised ML model capable of incremental training/learning

There are 3 user-defined hyperparameters:

1. `SUPERVISED_PROBA_THRESH`: utterances whose predicted probability of being an existing `'final'` label is below this threshold will be given a new tentative label.
2. `UNSUPERVISED_COS_DIST_THRESH`: Utterances farther than this from an existing tentatively-labeled utterance will be given a newly-generated label 
3. `UNSUPERVISED_NGRAM_LEN_WEIGHT`: Weight applied N-gram lengths in computing the weighted average with N-gram encoding distance from utterance encoding.

## Pipeline (one utterance at a time)

1. Use SBERT + pretrained `LogisticRegression` to classify the utterance.
2. Label will be `'unknown'` or one of the existing `'final'` labels.
  - `IF` predicted label is `'unknown'` or `predict_proba()` is less than user-defined `SUPERVISED_PROBA_THRESH`:
    - `IF` SBERT encoding vector is closer than user-defined `UNSUPERVISED_COS_DIST_THRESH` to any `'tentative'` label(s) (user-defined threshold):
      - 1. Apply the closest tentative label to the utterance
      - 2. Add the utterance plus tentative label to the table of labeled utterances
      - 3. Mark the labeled-utterance entry with the label status `tentative`. 
    - `ELSE` (far from any `'final'` or `'tentative'`ly labeled utterance):
      - 1. generate all possible 1-grams, 2-grams, and 3-grams from the unlabeled utterance
      - 2. encode all the N-grams with SBERT and measure cosine distance to utterance encoding vector
      - 3. compute the N-gram (tentative label) loss as the weighted (user-specified `UNSUPERVISED_NGRAM_LEN_WEIGHT` and `(1 - UNSUPERVISED_NGRAM_LEN_WEIGHT)`) average of the N-gram length (in characters) and the cosine distance from the utterance
      - 4. find the minimum loss label
      - 5. lowercase it and replace whitespace with underscores (`_`) to construct a new tentative label
      - 6. apply the tentative label to the utterance
      - 7. add the new utterance plus best tentative label pair to the table of labeled utterances (make sure each row of this table can be identified as either `'tentative'` or `'final'`)
  - ELSE (supervised classifier `.predict_proba()` is sufficiently high based on user-defined parameter `SUPERVISED_PROBA_THRESH`):
      - 

## Translation

##
