## MAITag Experiment/Model tracking

Goal: Build a tool for ML dataset and experiment tracking.

For an example of an open source system that we want to imitate, play with MLFlow on Rori's models to see what it does well and what views we might want to add to MAITag.

User Story/Epic: A machine learning engineer on a chatbot project would like to be able to:

- CRUD (create, read, update, delete) joblib `*.pkl` files containing model pipelines such as the Rori intent_recognizer in the `mathtext` package
- models.py data structure for storing models and labeled datasets for chatbot intent recognizer models
- endpoint for downloading latest version of any model (pkl file) in MAITag
- endpoint for downloading labeled message datasets from MAITag
- endpoint for doing inference (intent recognition/tagging) using a particular model in MAITag

### Task ideas

- deploy MLFlow to Render
- MAITag - get the new `multilabel_intent_recognizer.joblib.pkl` file working (Vlad can assist)
- get MAITag working with digital ocean spaces for storing and retrieving pkl files for models
- check that developers (Alex, Vlad, John) ssh keys work with MAITAG on render
- give Alex and Vlad Digital Ocean Spaces tokens/keys/accounts
- intent recognition endpoint similar to Greg's mathtext-fastapi/nlu/ endpoint(s)
- model selection in admin interface
- user_id fk for each model
- `LabeledMessage` table 
- `M2M LabeledMessage` to `Label` (tags) table 
- import Google Sheets for ["NLU Labeled Dataset" **Intent** tab](https://docs.google.com/spreadsheets/d/12pTKptw_VE5D1PmYWlm-GFGaaEKPgTYfddax7NgLVDs/edit#gid=256991159) CSV into LabeledMessage table
- import Google Sheets for ["NLU Labeled Dataset" **Labels** tab"](https://docs.google.com/spreadsheets/d/12pTKptw_VE5D1PmYWlm-GFGaaEKPgTYfddax7NgLVDs/edit#gid=1088636651) into the `Label` table (models.py) in MAITag.
- user_id fk for each labeled message
- view that lists all models on a user's account for user to user profile to select model