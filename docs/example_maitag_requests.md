# MAITAG Requests Example

Go to the terminal, and run `ipython` to test the label endpoint and make predictions on utterance:

## Dependencies

```python
import requests
```

```python
url = 'https://maitag.onrender.com/api/maitag/label/'
```

## Example utterance 1 

```python
data = {"utterance": "how was your day"}
```

Making a request: 

```python
response_1 = requests.post(url, json=data)
response_1.text
```
Response:

```python
'{"project_id":null,"utterance_id":null,"utterance":"how was your day","label":"how_are_you", "confidence":0.98}'
```

## Example utterance 2

```python
data = {"utterance": "rochdi is in milkyway"}
```

Making a request:

```python
response_2 = requests.post(url, json=data)
response_2.text
```
Response:

```python
'{"project_id":null,"utterance_id":null,"utterance":"rochdi is broken","label":"unknown","confidence":0.93}'
```
