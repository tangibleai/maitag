# Sprint 12 (Nov 04 - Nov 11)

* [x] R1-H4: Add an automatic test to test the `predict_unknown_label()` during the build
* [x] R2-2: Test and play around with `django-userna-ce` package
* [x] R1-0.5: Write a markdown file to describe the features/issues the package has
* [x] R0.1-0.1: Create a new Tangible AI repository called `django-tangauth` and clone it
* [x] R0.1-0.1: Create an empty Django project named `core`
* [x] R0.5-0.5: Copy `maitag/users` app and paste it into the newly created repository
* [x] R2-1.5: Make `maitag/users` app reusable Django app using this this [tutorial](https://docs.djangoproject.com/en/4.1/intro/reusable-apps/)
* [x] R1-0.8: Test the `django-tangauth` package in a new Django project
* [x] R0.2-0.1: Create a new tab called label in Nurse Nisa project
* [x] R0.2-0.1: Add a table within the newly created tab
* [x] R0.5-0.5: Add a dropdown menu and button to send POST requests to the `api/maitag/label` endpoint
* [x] R0.2-0.5: Fill the prediction and confidence columns with the predicted label
* [x] R0.5-0.5: Create the same tag in the new instance of Delvin on ToolJet

## Done: Sprint 11 (Oct 28 - Nov 04)

* [x] R1-0.5: Create a README file for `maitag`
* [x] R2-2: Take another practical tutorial to get familiar with ToolJet
* [x] R2-1: Send an API request to `/api/maitag/label` endpoint when the user assigns an intent
* [x] R0.5: Create a new execrice for the next ToolJet task

