# Sprint 14 (Nov 18 - Nov 25)

* [] R1: Take a tutorial to learn how to use Django Sites Framework (DSF)
* [] R2: Edit the authentication views with forms and their corresponding templates
* [] R0.5: Create a merge request to merge `feature-dsf-support` into `main` 
* [] R1: Modularize the `predict_label()` to have two functions that predict one single utterance and multiple utterances
* [] R0.5: Create a pull/merge request to merge the feature and assign it to Hobson
* [] H0.2: Have Hobson send you the sendgrid tokens via Bitwarden

## Done: Sprint 13 (Nov 11 - Nov 18)

* [x] R0.2-0.1: Use this Stackoverflow [thread](https://stackoverflow.com/questions/5843580/auth-profile-module-in-django) to learn how to use the `AUTH_PROFILE_MODULE` setting
* [X] R0.5-1: Figure out how to implement the Team model for `django-delvin`
* [x] R0.2: Create a branch called `feature-dsf-support` 
* [x] R1: Enable the DSF in `django-delvin` project