# Sprint 6 (Sep 23 - Sep 30

* [x] R1-1: Read/understand what word embedding is and why we need it
* [x] R1-1: Run/understand the cells that do encoding, splitting, training, and some testing
* [x] R0.5-0.2: Filter out the suspicious/unknown rows
* [x] R0.2-0.2: Group the dataframe by intent and highlight the first utterance in every intent
* [x] R0.5-0.2: Update the intent column by adding some corrections
* [x] R1-0.5: Revise the unknown values
* [x] R1-0.5: Overwrite the intent column with the new revised column `your_corrected`
* [x] R0.2-0.2: Save the file as a CSV file 
* [x] R0.2-0.2: Train and test the model with the new corrected data
* [x] R0.1-0.1: Share with your team your Hobson's MAYA notebook copy

## Done: Sprint 5 (Sep 16 - Sep 23)

* [x] R0.1: Copy Hobson's Jupyter notebook that process MAYA messages
* [x] R2-2: Run and understand what each cell is doing in the notebooks