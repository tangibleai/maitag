# Sprint 11 (Oct 28 - Nov 04)

* [x] R1-0.5: Create a README file for `maitag`
* [x] R2-2: Take another practical tutorial to get familiar with ToolJet
* [x] R2-1: Send an API request to `/api/maitag/label` endpoint when the user assigns an intent
* [x] R0.5: Create a new execrice for the next ToolJet task

## Done: Sprint 10 (Oct 21 - Oct 28)

* [x] R2-2: Merge the authentication API endpoints into the `django-delvin/users` app
* [x] R1-1: Merge the API endpoints of `maitag` into the `django-delvin/maitag` app
* [x] R0.5-0.2: Add the `maitag`'s home page templates to `django-delvin/maitag` app
* [x] R1-0.5: Test the API endpoints in production
* [x] R0.5-0.1: Contact Maria for the tooljet/javascript widget frontend task
* [x] R2-2.2: Take a tutorial on ToolJet to get familiar with the framework
* [x] R0.5-0.2: Connect to Maria's database using the credentials on Bitwarden
* [x] R1-0.9: Browse the database and understand its structure
* [x] R1-0.8: Read and understand the [feature machine assistance](https://gitlab.com/tangibleai/maitag/-/blob/main/docs/feature-machine-assistance.md) doc
* [x] R0.5-0.1: Schedule a meeting with Vish to discuss `maitag` and next features