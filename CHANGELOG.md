# MAITAG Release Notes

## 0.1.1
```yaml
0.1 = supervised model trained on the maya labeled dataset v1
3:40 PM
...2 = unsupervised model that vish created (edited) 
3:41 PM
0.1.1 was the supervised maya labels plus the "first word" unsupervised model that I created for the API testing
```

## 0.1.2
```yaml
model_name: maya
model_version: v0.1.2
model_description:
    supervised: 2022-09-01 LogisticRegression trained on ~1000 labeled Maya user utterances 
    unsuperbised: Vish named entity extraction unsuperbised label generator with a BERT cosine similarity threshold of .7?.
git_refspec: 
git_tag:
```
