from maitag_app.ml import encode, predict_labels
import pandas as pd
from pathlib import Path
from tqdm import tqdm


DATA_DIR = Path('data') / 'private'
LOG_FILEPATH = DATA_DIR / 'label_maya_utterances.log.jsonl'
UTTERANCE_FILENAMES = 'utterances_1.csv utterances_2.csv'.split()

dfs = []
for k, fn in enumerate(UTTERANCE_FILENAMES):
    fp = DATA_DIR / fn
    df = pd.read_csv(fp)
    df.columns = [c.strip() for c in df.columns]
    dfs.append(df)
df = pd.concat(dfs, axis=0)

# encode (vectorize) all the utterances to memoize/cache all the embeddings
utt_enc_dict = {}
for utt in tqdm(df['english']):
    utt_enc_dict[utt] = [float(x) for x in encode(utt)]

# # 10 utterances can be embedded per second on my laptop so backing up this data isn't necessary
# import json
# with open('utt_enc_dict.json', 'w') as fout:
#     json.dump(utt_enc_dict, fout)

#
label_conf = predict_labels(list(df['english']), progressbar=tqdm)
