#!/usr/bin/env bash
source .env

# Create a superuser

python manage.py shell <<EOF

from users.models import CustomUser

CustomUser.objects.create_superuser(
    username = '$SUPER_USERNAME',
    email = '$SUPER_EMAIL',
    password = '$SUPER_PASSWORD'
)

exit()

EOF
