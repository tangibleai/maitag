import requests
import json
import pandas as pd
from pathlib import Path
from tqdm import tqdm
import jsonlines


DATA_DIR = Path('data') / 'private'
URL = 'http://127.0.0.1:8000/api/maitag/label/'
FILEGLOB = 'utterances_*.csv'
LOG_FILEPATH = DATA_DIR / 'label_maya_utterances.log.jsonl'


if __name__ == '__main__':
    dfs = []

    results_dfs = []
    results = []

    cache = dict()
    if LOG_FILEPATH.is_file():
        jsl = list(jsonlines.open(LOG_FILEPATH, 'r').iter())
        if len(jsl):
            cache_df = pd.DataFrame(jsl)
            cache = pd.Series(
                cache_df['label'].str.strip(),
                index=cache_df['utterance'].str.strip()
            ).to_dict()

    with LOG_FILEPATH.open('a+') as logfile:
        log = jsonlines.Writer(logfile)
        for k, fp in enumerate(DATA_DIR.glob(FILEGLOB)):
            df = pd.read_csv(fp)
            for i, row in tqdm(df.iterrows(), total=len(df)):
                utterance_id = f'{fp}-{i}'
                utterance = row['english']
                confidence = 1
                data = dict(
                    utterance_id=utterance_id,
                    utterance=utterance)
                resp_dict = data.copy()
                label = cache.get(utterance)
                if label is not None:
                    resp_dict['label'] = cache[utterance]
                else:
                    resp = requests.post(url=URL, data=data)
                    resp_dict = json.loads(resp.text)
                    label = resp_dict.get('label')
                    if label is not None:
                        cache[utterance] = label
                log.write(results[-1])
                results.append(resp_dict)
            try:
                results_dfs.append(pd.concat([df, pd.DataFrame(results)], axis=1))
            except Exception as e:
                print(e)

    results_df = pd.concat(results_dfs)
    results_path = DATA_DIR / FILEGLOB.replace('*', '_star_').replace('?', '_qmrk_')
    results_path = results_path.with_suffix('.maitag.csv')
    results_df.to_csv(results_path, index=False)

    results_df = results_dfs[-1]
    print(results_df)

    df1 = pd.read_csv(DATA_DIR / 'utterances_1.csv')
    df1['label'] = results_dfs[0]['label']
    df1['confidence'] = results_dfs[0]['confidence']
    df1.to_csv(DATA_DIR / 'utterances_1.labeled.csv', index=False)

    df2 = pd.read_csv(DATA_DIR / 'utterances_2.csv')
    df2['label'] = results_dfs[1]['label']
    df2['confidence'] = results_dfs[1]['confidence']
    df2.to_csv(DATA_DIR / 'utterances_2.labeled.csv', index=False)
