#!/usr/bin/env bash
source .env

if [ ! -f ".venv/bin/activate" ]; then
    pip install --upgrade pip poetry wheel virtualenv
    python -m virtualenv .venv
fi

if [ ! -n $(python -c 'from pathlib import Path; print(len(list(Path.cwd().glob(".venv/lib/python3.9/site-packages/maitag-*"))))') ] ; then 
    echo 'Installing maitag';
else 
    echo "maitag already installed";
fi

if [ "$RENDER" == "True" ]; then
    echo "reinstalling maitag because RENDER=$RENDER and running build.sh on RENDER"
    source .venv/bin/activate
    pip install -e .
fi


if [ "$RENDER" != "True" ]; then
    echo "Running local build.sh (make migrations)"
    python manage.py makemigrations --no-input
fi

python manage.py collectstatic --no-input
echo "Finished collecting static files"
python manage.py migrate
echo "Finished migrations"
source scripts/create_superuser.sh
echo "Finished creating superuser"

# python ./maitag_app/download_models.py
# OR
# python -m maitag_app.download_models.py
python -m spacy download en_core_web_md
echo "Finished downloading en_core_web_md for SpaCy"
python -c 'from sentence_transformers import SentenceTransformer; sbert = SentenceTransformer("paraphrase-MiniLM-L6-v2"); print(sbert)'
echo "Finished downloading SBERT for sentence_transformers"