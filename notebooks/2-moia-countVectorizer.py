# Dependencies
from sklearn.feature_extraction.text import CountVectorizer
import pandas as pd  



# Initialize the CountVectorizer() object
vectorizer = CountVectorizer()

# Read the document
df = pd.read_csv('../data/public/utterance_intent_pairs_revised.anon.csv')
print(df.head())

# Extract the MAYA messages
mayaMessages = df['utterance']

# Fit our document to preprocess it, tokenize it, and vectorize it
X = vectorizer.fit_transform(mayaMessages)

# Vocabulary
print(vectorizer.get_feature_names_out())

# BOW (bag of words)
mayaBow = X.toarray()
print(mayaMessages[0])
print(mayaBow[0]) # check the BOW of the first document "*menu*"