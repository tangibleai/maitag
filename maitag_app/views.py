from django.shortcuts import render
from django.views.generic import FormView
from rest_framework import generics
from rest_framework import status
from rest_framework.response import Response

from .forms import LabelForm
from .models import (
    Utterance,
    Intent,
    LabelInstance,
    MLModel,
    Classifier,
    Project
)
from .serializers import (
    UtteranceSerializer,
    IntentSerializer,
    LabelInstanceSerializer,
    MLModelSerializer,
    ClassifierSerializer,
    ProjectSerializer
)

from .ml import predict_labels


def home(request):
    return render(request, 'maitag_app/home.html')


class MLModelList(generics.ListCreateAPIView):

    queryset = MLModel.objects.all()
    serializer_class = MLModelSerializer


class MLModelDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = MLModel.objects.all()
    serializer_class = MLModelSerializer


class ClassifierList(generics.ListCreateAPIView):

    queryset = Classifier.objects.all()
    serializer_class = ClassifierSerializer


class ClassifierDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = Classifier.objects.all()
    serializer_class = ClassifierSerializer


class IntentList(generics.ListCreateAPIView):

    queryset = Intent.objects.all()
    serializer_class = IntentSerializer


class IntentDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = Intent.objects.all()
    serializer_class = IntentSerializer


class LabelInstanceList(generics.ListCreateAPIView):

    queryset = LabelInstance.objects.all()
    serializer_class = LabelInstanceSerializer


class LabelInstanceDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = LabelInstance.objects.all()
    serializer_class = LabelInstanceSerializer


class UtteranceList(generics.ListCreateAPIView):

    queryset = Utterance.objects.all()
    serializer_class = UtteranceSerializer


class UtteranceDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = Utterance.objects.all()
    serializer_class = UtteranceSerializer


class ProjectList(generics.ListAPIView):

    queryset = Project.objects.all()
    serializer_class = ProjectSerializer


class ProjectDetail(generics.RetrieveAPIView):

    queryset = Project.objects.all()
    serializer_class = ProjectSerializer


class Label(generics.CreateAPIView):

    queryset = Project.objects.all()
    serializer_class = ProjectSerializer

    def post(self, request):
        serializer = ProjectSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            utterance = serializer.data['utterance']
            predictions = predict_labels(utterance)
            data = {
                "project_id": serializer.data['project_id'],
                "utterance_id": serializer.data['utterance_id'],
                "utterance": utterance,
                "label": predictions['label'],
                "confidence": round(predictions['confidence'], 2)
            }
            return Response(data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LabelForm(FormView):
    # template_name = 'utterance_label_form.html'
    form_class = LabelForm
    success_url = '/thanks/'

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        form.send_utterance_label()
        return super().form_valid(form)


""" Concrete View Classes
# CreateAPIView
Used for create-only endpoints.
# ListAPIView
Used for read-only endpoints to represent a collection of model instances.
# RetrieveAPIView
Used for read-only endpoints to represent a single model instance.
# DestroyAPIView
Used for delete-only endpoints for a single model instance.
# UpdateAPIView
Used for update-only endpoints for a single model instance.
# ListCreateAPIView
Used for read-write endpoints to represent a collection of model instances.
RetrieveUpdateAPIView
Used for read or update endpoints to represent a single model instance.
# RetrieveDestroyAPIView
Used for read or delete endpoints to represent a single model instance.
# RetrieveUpdateDestroyAPIView
Used for read-write-delete endpoints to represent a single model instance.
"""
