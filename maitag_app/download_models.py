# download_models.py
import os

if not os.environ.get('DJANGO_SETTINGS_MODULE'):
    os.environ['DJANGO_SETTINGS_MODULE'] = 'maitag_site.settings'
# from django.conf import settings  # noqa
# settings.configure()
from django.core.cache import cache


SBERT_NAME = 'paraphrase-MiniLM-L6-v2'
ENCODER = SBERT_NAME
sbert = cache.get(SBERT_NAME)
if sbert is None:
    from sentence_transformers import SentenceTransformer
    sbert = SentenceTransformer(SBERT_NAME)
    cache.set(SBERT_NAME, sbert, None)  # None is the timeout parameter. It means cache forever


NLP_NAME = 'en_core_web_md'
nlp = cache.get(NLP_NAME)
if nlp is None:
    import spacy
    try:
        nlp = spacy.load(NLP_NAME)
    except OSError:
        spacy.cli.download(NLP_NAME)
        nlp = spacy.load(NLP_NAME)
    cache.set(NLP_NAME, nlp, None)  # None is the timeout parameter. It means cache forever


def encoder(s):
    return sbert.encode(s)


if ENCODER.lower() in ('spacy', NLP_NAME):
    def encoder(s):  # noqa
        return nlp(s).vector


if __name__ == '__main__':
    import pandas as pd
    print(pd.Series(encoder("Hello world.")))
