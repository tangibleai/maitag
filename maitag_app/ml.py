import logging
import numpy as np
import os
from pathlib import Path
import pickle
from sentence_transformers import util
import pandas as pd
import jsonlines


log = logging.getLogger()

if not os.environ.get('DJANGO_SETTINGS_MODULE'):
    os.environ['DJANGO_SETTINGS_MODULE'] = 'maitag_site.settings'
# from django.conf import settings  # noqa
# settings.configure()
from django.core.cache import cache
from maitag_app.download_models import sbert, nlp

STATIC_PATH = Path(__file__).parent / 'static'
MODEL_NAME = 'model_v1'
MODEL_PATH = STATIC_PATH / 'maitag_app' / 'trained_models' / (MODEL_NAME + '.pkl')
UTTERANCE_EMBEDDINGS_PATH = STATIC_PATH / 'maitag_app' / 'trained_models' / 'UTTERANCE_EMBEDDINGS.jsonl'

UNSUPERVISED_SCORE_THRESH = 0.55
SUPERVISED_PROBA_THRESH = 0.45
GENERATED_UNSUPERVISED_CONF_MAX = 0.2
GENERATED_UNSUPERVISED_CONF_MIN = 0
GENERATED_UNSUPERVISED_CONF_MINMAX_RANGE = GENERATED_UNSUPERVISED_CONF_MAX - GENERATED_UNSUPERVISED_CONF_MIN
EMBEDDING_DTYPE = np.float32

try:
    assert MODEL_PATH.is_file()
except AssertionError:
    print(f"{MODEL_PATH} does not exist??")
    raise

# os.environ['DJANGO_SETTINGS_MODULE'] = 'maitag_site.settings'

# set and get your trained model from the cache
# get model from cache


# the dictionary below is for unknown utterances and new intent labels only
dict_of_utterances_to_labels = {}

model = cache.get(MODEL_NAME)
if model is None:
    with open(MODEL_PATH, 'rb') as finb:
        model = pickle.load(finb)
        model.class_names = list(model.classes_)
        # set the model in the cache and save it
        cache.set(MODEL_NAME, model, timeout=None)  # None is the timeout parameter. It means cache forever

CLUSTERED_UTTERANCES_DICT = {}
UTTERANCE_EMBEDDINGS = {}
if not UTTERANCE_EMBEDDINGS_PATH.is_file():
    with jsonlines.open(UTTERANCE_EMBEDDINGS_PATH, 'w') as jslwriter:
        jslwriter.write_all(UTTERANCE_EMBEDDINGS)


UTTERANCE_EMBEDDINGS = cache.get(UTTERANCE_EMBEDDINGS_PATH)
if UTTERANCE_EMBEDDINGS is None:
    try:
        with jsonlines.open(UTTERANCE_EMBEDDINGS_PATH, 'r') as jsonlreader:
            UTTERANCE_EMBEDDINGS = dict(
                (line[0], np.array(line[1]))
                for line in jsonlreader.iter() if len(line)
            )
    except FileNotFoundError:
        UTTERANCE_EMBEDDINGS = {}
cache.set(UTTERANCE_EMBEDDINGS_PATH, UTTERANCE_EMBEDDINGS, timeout=None)


def generate_intent_name(utterance):
    """ Generate a snake_case formatted str with suggested intent name or summary of the utterance

    >>> generate_intent_name(utterance='I went to San Diego')
    'san_diego'
    >>> generate_intent_name(utterance='A gigafactory in China')
    'china'
    >>> generate_intent_name(utterance='Prosocial impact')  #
    'prosocial'
    >>> generate_intent_name(utterance='San Diego tech coffee is fun!')  #
    'san_diego'
    """
    current_labels = list(dict_of_utterances_to_labels.values())
    doc = nlp(utterance)
    suggested_intents = []
    noun_chunks = list(doc.noun_chunks)
    for chunk in noun_chunks:
        suggested_intent = [str(token).lower() for token in chunk if token.pos_ == "PROPN"]
        if len(suggested_intent) > 0:
            suggested_intents.append("_".join(suggested_intent))

    intent_name = None
    if len(suggested_intents) == 0:
        intent_name = str(doc[0]).lower()
    else:
        intent_name = max(suggested_intents, key=len)

    if intent_name not in current_labels:
        return intent_name
    else:
        return str(len(current_labels) + 1)


def encode(sentence):  # , utterance_embeddings=UTTERANCE_EMBEDDINGS):
    """ Memoized `sbert.encode()` function """
    sentence = sentence.strip() if isinstance(sentence, str) else ''

    # retrieve cached embedding if available
    vector = UTTERANCE_EMBEDDINGS.get(sentence)
    if vector is not None:
        vector = np.array(vector).astype(EMBEDDING_DTYPE)
        print(f'Found cached embedding for "{sentence[:16]}...": {vector.shape}')
        return vector

    # encode new utterance (create new embedding vector)
    vector = np.array(sbert.encode(sentence)).astype(EMBEDDING_DTYPE)
    print(f'New embedding for "{sentence[:32]}...": {vector.shape}')
    UTTERANCE_EMBEDDINGS[sentence] = vector
    cache.set(UTTERANCE_EMBEDDINGS_PATH, UTTERANCE_EMBEDDINGS, timeout=None)
    with jsonlines.open(UTTERANCE_EMBEDDINGS_PATH, 'a') as jsonlwriter:
        jsonlwriter.write([sentence, [float(x) for x in vector]])
    return vector


def classify_unknown_intent(new_utterance, clustered_utterances_dict=CLUSTERED_UTTERANCES_DICT):
    """ Use LM to agglomeratively (incrementally) cluster utterances

    >>> classify_unknown_intent('I went to San Diego')
    'san_diego'
    >>> classify_unknown_intent('A gigafactory in China')
    'china'
    >>> classify_unknown_intent('Prosocial impact')  #
    'prosocial'
    >>> classify_unknown_intent('San Diego tech coffee is fun!')  #
    'san_diego'
    >>> classify_unknown_intent("China is blackmailing Melon Husk's gigafactory")
    'china'
    """
    if not isinstance(new_utterance, str):
        log.warning(f"Cannot classify non-string utterances: {type(new_utterance)}({new_utterance})")
        new_utterance = str(new_utterance)
    label_centroid_dict = {}
    for label, utterances in clustered_utterances_dict.items():
        label_centroid_dict[label] = np.array(
            [encode(utt) for utt in utterances]
        ).mean(axis=0)

    max_score = 0
    label_with_max_score = None

    utterance_embedding = encode(new_utterance).astype(EMBEDDING_DTYPE)
    for label, cluster_centroid in label_centroid_dict.items():
        try:
            score = util.pytorch_cos_sim(
                cluster_centroid, utterance_embedding)[0].numpy()[0]
        except Exception as e:
            print(f"util.pytorch_cos_sim(old, new): type(newembd), newutt: {type(utterance_embedding)} '{new_utterance[:16]}...'")
            print(e)
        if score > max_score and score >= UNSUPERVISED_SCORE_THRESH:
            max_score = score
            label_with_max_score = label

    if label_with_max_score is not None:
        clustered_utterances_dict[label_with_max_score].append(new_utterance)
    else:
        label_with_max_score = generate_intent_name(new_utterance)
        max_score = GENERATED_UNSUPERVISED_CONF_MAX
        clustered_utterances_dict[label_with_max_score] = [new_utterance]
    return (
        label_with_max_score,
        GENERATED_UNSUPERVISED_CONF_MIN + max_score * GENERATED_UNSUPERVISED_CONF_MINMAX_RANGE
    )


def predict_label(utterance, progressbar=None):
    """ Predict the intent label (category) for a single utterance (str) using SBERT

    >>> predict_label('bye')
    {'label': 'goodbye', 'confidence': ...}
    """
    return predict_labels([utterance], progressbar=progressbar)[0]


def predict_labels(utterances, progressbar=None):
    r""" Use pretrained LogisticRegression and SBERT to predict intent labels

    >>> predict_labels(['bye', 'hello world!'])
    [{'label': 'goodbye', 'confidence': ...}, {'label': 'hello', 'confidence': ...}]
    >>> predict_labels('bye')
    {'label': 'goodbye', 'confidence': ...}
    """
    progressbar = progressbar or list
    single = False
    if isinstance(utterances, (str, bytes)):
        single = True
        if isinstance(utterances, str):
            utterances = [utterances]
        else:
            utterances = [utterances.decode()]
    try:
        utterances = list(utterances)
    except TypeError:
        single = True
        utterances = [str(utterances)]

    encoded_utterances = [encode(utt) for utt in utterances]
    labels = model.predict(encoded_utterances)
    # label = list(prediction)[0]
    # class_index = np.where(model.classes_ == label)
    probas = model.predict_proba(encoded_utterances)
    predictions = []
    for label, ps, utterance in progressbar(zip(labels, probas, utterances)):
        class_num = model.class_names.index(label)
        proba = ps[class_num]
        if label == 'unknown' or proba < SUPERVISED_PROBA_THRESH:
            new_intent, new_conf = classify_unknown_intent(utterance)
            predictions.append([new_intent, new_conf])
        else:
            predictions.append([label, proba])
    predictions = [
        dict(label=lbl, confidence=conf) for lbl, conf in predictions]
    if single:
        return predictions[0]
    return predictions


TEST_UTTERANCES = [
    'tell me more about it',
    'how was your day?',
    'can I ask you something?',
    'how are you?',
    'bye',
    'hello world!,',
    'How do you work? Are you a robot that uses ChatGPT?',
    'sup',
    'What is up?',
    "What's up?",
    "Have you chatted with ChatGPT before?",
    "Have you chatted with Chat GPT before?",
]


def test_predict_labels(utterances=TEST_UTTERANCES):
    labels = predict_labels(utterances)
    df = pd.DataFrame(labels, index=utterances)
    print(df)
    # assert len(set(labels)) < 3
    return df


if __name__ == '__main__':
    import doctest
    doctest.testmod()
    df = test_predict_labels()
