from django import forms


class LabelForm(forms.Form):
    utterance = forms.CharField(initial='type your utterance here', max_length = 200)
    label = forms.CharField(initial='', max_length=128)

    def send_utterance_label(self):
        # requests.post(...)
        pass
