from django.contrib import admin
from .models import (
    MLModel, 
    Classifier, 
    Intent, 
    LabelInstance,
    Utterance,
    Project
)



admin.site.register(MLModel)
admin.site.register(Classifier)
admin.site.register(Intent)
admin.site.register(LabelInstance)
admin.site.register(Utterance)
admin.site.register(Project)
