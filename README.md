# MAITAG

MAITAG is a collection of Machine learning models and API for natural language intent classification.

Try it live on: https://maitag.onrender.com/

## Installation

Follow the instructions below to run `maitag` properly on your machine:

1. Clone the repository and go to the project folder:

```bash
git clone git@github.com:tangibleai/maitag.git
cd maitag
```
2. Create a new virtual environment to install the project requirements:

```bash
pip install -U --upgrade pip virtualenv
python -m virtualenv venv
source venv/bin/activate
pip install -e .
```

3. Make and run migration files to set up the project database:

```bash
python manage.py makemigrations
python manage.py migrate
```

## Run Development Server

Run the following command to start using `maitag`:

```bash
python manage.py runserver
```

## Production

Follow the instructions below to deploy `maitag` on Render:

1. Create a Django web service to deploy `maitag` with these settings
2. Use `./build.sh` as a build command
3. Run `maitag` with Gunicorn and use `gunicorn maitag_site.wsgi:application` as a start command
4. Add your environment variables and deploy

