""" Hard coded configuration values such as HOME_DIR and DATA_DIR that never change """

import re
import logging
from pathlib import Path

import json  # noqa
import spacy  # noqa

__version__ = None
# from maitag import __version__  # noqa

# LOGGING_FORMAT = '%(asctime)s.%(msecs)d %(levelname)-4s %(filename)s:%(lineno)d %(message)s'
LOGGING_FORMAT = "%(levelname)-4s %(filename)-16s:%(lineno)4d %(funcName)-16s() %(message)s"
# LOGGING_DATEFMT = '%Y-%m-%d:%H:%M:%S'
LOGGING_DATEFMT = '%D:%H:%M:%S'
LOGGING_LEVEL = logging.ERROR
logging.basicConfig(
    format=LOGGING_FORMAT,
    datefmt=LOGGING_DATEFMT,
    level=logging.ERROR)
log = logging.getLogger(__name__)
log.setLevel(LOGGING_LEVEL)
root_logger = logging.getLogger()

HOME_DIR = Path.home()
SRC_DIR = Path(globals().get("__file__", Path.cwd())).resolve().absolute().parent
BASE_DIR = SRC_DIR.parent
REPO_DIR = BASE_DIR

DATA_DIR = Path(BASE_DIR, 'data')
DATA_DIR.mkdir(exist_ok=True, parents=True)
LOG_DIR = Path(DATA_DIR, 'log')
LOG_DIR.mkdir(exist_ok=True, parents=True)
CONSTANTS_DIR = DATA_DIR / 'constants'
CONSTANTS_DIR.mkdir(exist_ok=True, parents=True)
HISTORY_PATH = DATA_DIR / 'history.yml'


while BASE_DIR.parent.name.startswith("mait") and len(str(BASE_DIR)) > 3:
    BASE_DIR = BASE_DIR.parent
DATA_DIR = BASE_DIR.parent / "maitag" / "data"
DATA_DIR_PRIVATE = DATA_DIR / "private"
DATA_DIR_PUBLIC = DATA_DIR / "public"

DBFILENAME = "utterance_intent_pairs_2022-10-12.csv"
FILEPATH_PRIVATE = DATA_DIR_PRIVATE / DBFILENAME
FILEPATH_PUBLIC = (DATA_DIR_PUBLIC / DBFILENAME).with_suffix('.anon.csv')
FILEPATH = FILEPATH_PRIVATE


def get_version():
    """ Look within setup.cfg for version = ... and within setup.py for __version__ = """

    with (REPO_DIR / 'pyproject.toml').open() as fin:
        for line in fin:
            matched = re.match(r'\s*version\s*=\s*([.0-9abrc])\b', line)
            if matched:
                return (matched.groups()[-1] or '').strip()

    # setup.py
    try:
        version = next(iter(
            line for line in (REPO_DIR / 'setup.py').open() if line.startswith('__version__ = ')))
        return version[len('__version__ = '):].strip('"').strip("'")
    except Exception as e:
        print('ERROR: Unable to find version in setup.py.')
        print(e)


__version__ = __version__ or get_version()
